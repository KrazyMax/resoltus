# NOTE: pas utile car il existe déjà un fichier dictionary.txt 
# dans le module French-Dictionary

import glob

with open("french.dic", 'w') as compiled_dic:
    for name in glob.glob("/Users/krazymax/Documents/_dev/resoltus/French-Dictionary/dictionary/*.txt"):
        with open(name, 'r') as dic:
            for line in dic.readlines():
                if ';' in line: compiled_dic.write(f"{line.split(';')[0]}\n")
                else: compiled_dic.write(line)
