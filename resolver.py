import os
from collections import Counter


class ResolverMngr():


    def __init__(self, nbr_letters, startwith=''):
        self.nbr_letters = nbr_letters
        self.startwith = startwith
        self.french_dict_file_name = "french_dictionary.txt"
        self.temp_file_name = "temp_words.txt"


    def parse_result(self, sutom_result):
        # sutom_result de la forme "x_r x x x_y x_y x_r"

        sutom_codes = sutom_result.split(' ')
        sutom_letters = [l.split('_')[0] for l in sutom_codes]
        unique_sutom_letters = list(set(sutom_letters))

        for index, code in enumerate(sutom_codes):
            sutom_result_letter = code.split('_')[0]
            if '_r' in code:
                self.letters_distribution['red'].append([sutom_result_letter, index])
            elif '_y' in code:
                self.letters_distribution['yellow'].append([sutom_result_letter, index])
            else:
                self.letters_distribution['blue'].append(sutom_result_letter)

        # les lettres wrong sont les blue n'étant ni red ni yellow
        self.letters_distribution['wrong'] = list(set([
            bl for bl in self.letters_distribution['blue']
            if bl not in [yl[0] for yl in self.letters_distribution['yellow']]
            and bl not in [rl[0] for rl in self.letters_distribution['red']]]))

        for sutom_letter in [l for l in unique_sutom_letters if l not in self.letters_distribution['wrong']]:

            # on ne considère que les lettres qui ne sont pas wrong
            red_letters_list = [rl[0] for rl in self.letters_distribution['red']]
            yellow_letters_list = [yl[0] for yl in self.letters_distribution['yellow']]
            red_yellow_letters_count = Counter(red_letters_list + yellow_letters_list)

            # if blue => exact or / or wrong
            # if not blue => min (red + yellow)
            if sutom_letter not in [bl[0] for bl in self.letters_distribution['blue']]:
                self.letters_count['min'].append((sutom_letter, red_yellow_letters_count[sutom_letter]))
            else:
                self.letters_count['exact'].append((sutom_letter, red_yellow_letters_count[sutom_letter]))


    def reset_distribution_and_count(self):
        self.letters_distribution = {
            'red': [],
            'yellow': [],
            'blue': [],
            'wrong': []
        }
        self.letters_count = {
            'exact': [],  # lettres apparaissant exactement n fois ( ex : [('x', 2)] )
            'min': []  # lettres apparaissant au moins n fois ( ex : [('x', 1)] )
        }


    def is_word_valid(self, word):

        # on élimine tous les mots qui ont des lettres wrong dedans
        for letter in self.letters_distribution['wrong']:
            if letter in word: return False

        # on élimine tous les mots qui n'ont pas de red au bon endroit
        for (letter, index) in self.letters_distribution['red']:
            if not letter == word[index]: return False

        # on élimine tous les mots :
        #   - qui n'ont pas de yellow dedans
        #   - qui ont des yellow au mauvais endroit
        for (letter, index) in self.letters_distribution['yellow']:
            if not letter in word or letter == word[index]: return False

        # on élimine tous les mots qui n'ont pas EXACTEMENT le nombre de lettres
        for (letter, exact_count) in self.letters_count['exact']:
            if word.count(letter) != exact_count: return False

        # on élimine tous les mots qui n'on pas AU MOINS le nombre de lettres
        for (letter, min_count) in self.letters_count['exact']:
            if word.count(letter) < min_count: return False

        return True


    def initialize_temp_file(self):
        # objectifs :
        #   - ne garder que les mots qui ont le bon nombre de lettres
        #   - éliminer les éventuels accents et autres caractères spéciaux
        #   - tout mettre en minuscule
        #   - éliminer les doublons
        with open(self.french_dict_file_name, "r") as all_french_words_file:
            with open(self.temp_file_name, 'w') as temp_file:
                before_word = ""
                for word in all_french_words_file.readlines():
                    word_to_process = word.split(',')[0].split(' ')[0].lower().replace('\n', '')
                    if len(word_to_process) == self.nbr_letters:
                        simple_word_to_process = word_to_process\
                            .replace('é', 'e').replace('è', 'e').replace('ê', 'e')\
                            .replace('â', 'a')\
                            .replace('ô', 'o')\
                            .replace('ï', 'i').replace('î', 'i')\
                            .replace('ç', 'c')\
                            .replace('û', 'u')
                        if simple_word_to_process == before_word: continue
                        else:
                            temp_file.write(simple_word_to_process + '\n')
                            before_word = simple_word_to_process


    def remove_words(self):
        with open("temp_words.txt", "r") as eligible_words_file:
            with open("new_temp_words.txt", "w") as new_eligible_words_file:
                for word in eligible_words_file.readlines():
                    if self.is_word_valid(word):
                        new_eligible_words_file.write(word)
        os.replace("new_temp_words.txt", "temp_words.txt")


    def get_richest_words(self):
        # renvoie une liste de mots maximisant le nombre de lettres à faire tomber
        richest_words = []
        max_length = 0
        with open(self.temp_file_name, "r") as eligible_words:
            for word in eligible_words.readlines():
                if self.startwith and self.startwith != word[0]: continue
                if len(set(word)) > max_length:
                    richest_words = [] + [word]
                    max_length = len(set(word))
                elif len(set(word)) == max_length:
                    richest_words.append(word)
            return richest_words


    def print_richest_words(self):
        print(''.join(self.get_richest_words()))


if __name__ == '__main__':

    while True:
        try:
            nbr_letters = int(input("Nombre de lettres à trouver ?\n"))
            break
        except Exception:
            print("On demande un nombre, pas des patates")
            pass

    while True:
        try:
            startwith = input("\nSi le mot commence par une lettre, l'indiquer ci-dessous, sinon appuyer juste sur entrer\n")
            if len(startwith) > 1 or int(startwith):
                print("Il faut UNE lettre, pas plusieurs, pas des bananes, bordel")
        except Exception:
            break

    resoltusMngr = ResolverMngr(nbr_letters=nbr_letters, startwith=startwith)
    resoltusMngr.initialize_temp_file()

    print("\nOn vous conseille de commencer par l'un des mots suivants :")
    resoltusMngr.print_richest_words()

    while True:
        sutom_result = input("\n\nRésultat du SUTOM codifié ?\n(_r si rouge, _y si jaune, rien sinon. Ex : m_r a n g_y e_y r pour manger\n")
        resoltusMngr.reset_distribution_and_count()
        resoltusMngr.parse_result(sutom_result)
        resoltusMngr.remove_words()
        print("\nOn vous conseille d'essayer l'un des mots suivants :")
        resoltusMngr.print_richest_words()
        